# Use an official PHP runtime as a parent image
FROM php:8.2-apache

# Install dependencies and PHP extensions
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    git \
    default-mysql-client \
    libpng-dev \
    zip \
    unzip \
    curl && \
    docker-php-ext-install pdo_mysql gd

# Set the working directory in the container
WORKDIR /var/www/html

# Copy your PHP application code into the container
COPY . .

# Expose the port Apache listens on
EXPOSE 80 443
